<!DOCTYPE html>
<head>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <?php $this->load->view("homeView"); ?>

<footer>

</footer>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/home.js"></script>
</body>
</html>
