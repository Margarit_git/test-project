<div class="container">


	<!-- Row start -->
	<div class="row">
		<div class="col-xs-12">
			<h2>Пожалуйста ответьте 3 вопросов</h2>
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<i class="icon-calendar"></i>
					<h3 class="panel-title">Вопросы</h3>
				</div>

				<div class="panel-body">
					<form class="form-horizontal row-border" action="#">
						<div class="form-group">
							<label class="col-md-3 control-label">Сколько цветов в радуге?</label>
							<div class="col-md-9">
								<label class="checkbox-inline">
									<input type="checkbox" class="answer_1_check" id="inlineCheckbox1" name="answer_1[]" value="1"> 2
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" class="answer_1_check" id="inlineCheckbox2" name="answer_1[]" value="2"> 7
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" class="answer_1_check" id="inlineCheckbox3" name="answer_1[]" value="3"> 14
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Сколько дней в году?</label>
							<div class="col-md-2 col-sm-9">
								<input class="form-control" type="text" name="answer_2">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Кто проживает на дне океана?</label>
							<div class="col-md-2 col-sm-9">
								<select class="form-control" name="answer_3">
									<option></option>
									<option value="spanchBob">Спанч боб</option>
									<option value="meduza">Медуза</option>
									<option value="rusalka">Русалка</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-2 col-sm-9">
								<button type="submit" id="sendAnswers" class="btn btn-info" title="">Отправить</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Row end -->

</div>
