<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h2 class="text-center">Результат ответов</h2>
			<div id="answersTable"></div>
			<div id="myChart" style="width: 700px; margin: 0 auto"></div>
		</div>
	</div>
</div>
<script>
	window._userAnswers = <?php echo json_encode($userAnswers); ?>;
</script>
