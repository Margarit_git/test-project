<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['questions'] = array(
	array(
		'id'      => '1',
		'name'    => 'Сколько цветов в радуге?',
		'type'    => 'checkbox',
		'answers' => array(
			1 => 2,
			2 => 7,
			3 => 14,
		),
	),
	array(
		'id'      => '2',
		'name'    => 'Сколько дней в году?',
		'type'    => 'text',
		'answers' => array(),
	),
	array(
		'id'      => '3',
		'name'    => '',
		'type'    => 'select',
		'answers' => array(
			'spanchBob' => 'Спанч боб',
			'meduza'    => 'Медуза',
			'rusalka'   => 'Русалка',
		),
	),
);
