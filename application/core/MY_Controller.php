<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 * @property BaseModel $BaseModel
 * @property UserAnswerModel $UserAnswerModel
 */

class MY_Controller extends CI_Controller
{

    /**
     * About main layout
     *
     */
    public $data = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
	 * Send response
     * @param $data array
	 * @return json
     */
	protected function response($data) {
    	echo json_encode($data);
    	exit;
	}

}

/**
 *
 * Class MY_Private_Controller
 *
 * @property BaseModel $BaseModel
 * @property UserAnswerModel $UserAnswerModel
 */
class MY_Private_Controller extends CI_Controller
{

    /**
     * About main layout
     *
     */
    public $data = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Send response
	 * @param $data array
	 * @return json
	 */
	protected function response($data) {
		echo json_encode($data);
		exit;
	}

}









