<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	private $_validCheckboxData = array();
	private $_validSelectdata = null;

    public function __construct(){
        parent::__construct();
		$this->load->config('questions');
    }

	/**
	 * The question page
	 */
    public function index(){
		$this->data['questions'] = $this->config->item('questions');
        $this->load->view('layout/default', $this->data);
    }

	/**
	 * Post request
	 */
    public function saveAnswers () {
    	$response = array('success' => false, 'message' => '', 'errors' => array());

        if($this->input->is_ajax_request()) {
        	$this->load->model('UserAnswerModel');
            $answers = $this->input->post(null, true);
			$this->form_validation->set_rules($this->UserAnswerModel->rules);

			if ($this->form_validation->run() == FALSE) {
				$response['errors'] = $this->form_validation->error_array();
				$this->response($response);
			}

            $userIpAddress = $this->_getClientIp();

			if ($this->input->cookie('user_answered', true)) {
				$response['message'] = 'Вы уже ответили!';
				$this->response($response);
			}

			$insertData = array(
				'answer_1' => implode(',', $this->_validCheckboxData),
				'answer_2' => $answers['answer_2'],
				'answer_3' => $this->_validSelectdata,
				'user_ip'  => $userIpAddress,
			);
			$result = $this->UserAnswerModel->insert($insertData);

			if ($result) {
				$response['result']  = $result;
				$response['success'] = true;
				$response['message'] = 'Спасибо, ваши результаты отправлены.';
				setcookie('user_answered', true);
			} else {
				$response['message'] = 'Пожалуйста, повторите попытку позже.';
			}

			$this->response($response);
        } else{
            redirect('/');
        }

    }

	/**
	 * Checking the answer one options are valid or not
	 * @param $data
	 * @return bool
	 */
	public function answer_1_check($data) {
		$this->_validCheckboxData = array();
		$questions = $this->config->item('questions');
		$answers = $questions[0]['answers'];
		$postData = $this->input->post('answer_1', true);
		if (is_array($postData) && !empty($postData)) {
			for ($i = 0, $l = count($answers); $i < $l; $i++){
				if (isset($postData[$i]) && isset($answers[$postData[$i]])) {
					$this->_validCheckboxData[] = $answers[$postData[$i]];
				}
			}
		}
		return count($this->_validCheckboxData) > 0;
	}

	/**
	 * Checking the answer one options are valid or not
	 * @param $data
	 * @return bool
	 */
	public function answer_3_check($data) {
		$questions = $this->config->item('questions');
		$answers = $questions[2]['answers'];
		$postData = $this->input->post('answer_3', true);
		if (isset($answers[$postData])) {
			$this->_validSelectdata = $answers[$postData];
		}
		return !!$this->_validSelectdata;
	}

	/**
	 * Get Client ip address
	 * @return array|false|string
	 */
    private function _getClientIp() {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
