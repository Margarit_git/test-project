<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Private_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model('UserAnswerModel');
    }

	/**
	 * The user answers list
	 */
    public function index(){
    	$userAnswers = $this->UserAnswerModel->getAll();
    	$groupedAnswers = array();
		foreach ($userAnswers as $key=>$answer) {
			if (isset($groupedAnswers[$answer->user_ip])) {
				$answer->total = $groupedAnswers[$answer->user_ip]->total + 1;
				$groupedAnswers[$answer->user_ip] = $answer;
			} else {
				$groupedAnswers[$answer->user_ip] = $answer;
				$groupedAnswers[$answer->user_ip]->total = 1;
			}
			$userAnswers[$key] = null;
			unset($userAnswers[$key]);
		}
		$userAnswers = array();
		foreach ($groupedAnswers as $key => $gAnswer) {
			$userAnswers[] = $gAnswer;
			$groupedAnswers[$key] = null;
			unset($groupedAnswers[$key]);
		}
    	$this->data['userAnswers'] = $userAnswers;
        $this->load->view('layout/admin/default', $this->data);
    }

    public function delete() {
		$response = array('success' => false, 'errors' => array());

		if($this->input->is_ajax_request()) {
			$this->load->model('UserAnswerModel');
			$answerIp = $this->input->post('answerIp', true);
//			echo $answerIp;exit;
			$where = array('user_ip' => $answerIp);
			$result = $this->UserAnswerModel->delete($where);
			if ($result) {
				$response['success'] = true;
			}
			$this->response($response);
		} else{
			redirect('/');
		}
	}
}
