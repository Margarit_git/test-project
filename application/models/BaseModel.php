<?php

class BaseModel extends CI_Model
{
	protected $table;

	function __construct() {
		parent::__construct();
	}

	/**
	 * @param $data
	 * @return boolean
	 */
	public function insert($data) {
		$this->db->insert($this->table, $data);
		return $this->db->affected_rows() > 0;
	}

	public function getAll(){
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function delete($where){
		$this->db->where($where);
		$this->db->delete($this->table);
		return $this->db->affected_rows() > 0;
	}
}
