<?php

class UserAnswerModel extends BaseModel
{
    /**
	 * Rules for validation
     * @var array
     */
    public $rules = array(
		array(
			'field' => 'answer_1',
			'label' => 'Сколько цветов в радуге?',
			'rules' => 'callback_answer_1_check',
			'errors' => array(
				'required' => '%s:  <strong>поле требуется.</strong>',
				'answer_1_check' => '%s: <strong>Выбранные параметры недействительны.</strong>',
			),
		),
		array(
			'field' => 'answer_2',
			'label' => 'Сколько дней в году?',
			'rules' => 'required|is_natural_no_zero|max_length[4]',
			'errors' => array(
				'required' => '%s: <strong>поле требуется.</strong>',
			),
		),
		array(
			'field' => 'answer_3',
			'label' => 'Кто проживает на дне океана?',
			'rules' => 'required|callback_answer_3_check',
			'errors' => array(
				'required' => '%s: <strong>поле требуется.</strong>',
				'answer_3_check' => '%s: <strong>Выбранные параметры недействительны.</strong>',
			),
		),
	);

    function __construct()
    {
        parent::__construct();
        $this->table = 'user_answers';
    }
}
