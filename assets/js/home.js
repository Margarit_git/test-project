(function($) {
	toastr.options.positionClass = "toast-top-center";
    $('#sendAnswers').click(function (e) {
        e.preventDefault();
        var formData  = $('form').serializeArray();
        $.ajax({
            url: "home/saveAnswers",
            data: formData,
            type: 'POST',
            success: function(response){
				response = JSON.parse(response);
				if (response.success) {
					toastr.success('', response.message);
				} else {
					if (response.errors.answer_1 || response.errors.answer_2 || response.errors.answer_3) {
						$.each(response.errors, function(index, message) {
							console.log(message);
							toastr.error('', message);
						});
					} else {
						toastr.error('', response.message);
					}
				}
            }
        });
    });
    $answer2 = $('input[name="answer_2"]');
    $answer3 = $('select[name="answer_3"]');

    $('#inlineCheckbox1').prop('checked', localStorage.getItem('inlineCheckbox1') === 'true');
    $('#inlineCheckbox2').prop('checked', localStorage.getItem('inlineCheckbox2') === 'true');
    $('#inlineCheckbox3').prop('checked', localStorage.getItem('inlineCheckbox3') === 'true');
	$answer2.val(localStorage.getItem('answer_2'));
	$answer3.val(localStorage.getItem('answer_3'));

    $('.answer_1_check').on('change', function(event) {
    	var id = $(this).attr('id');
		localStorage.setItem(id, $(this).is(':checked'))
	});

	$answer2.on('change', function(){
		localStorage.setItem('answer_2', $(this).val());
	});
	$answer3.on('change', function(){
		localStorage.setItem('answer_3', $(this).val());
	});

})(jQuery);
