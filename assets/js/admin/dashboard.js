(function($) {
	var answersTable = {};
	var makeAnswersTable = function(dbdata) {
		var table = $('<table class="table table-striped table-bordered dataTable no-footer"></table>');
		$('#answersTable').empty().append(table);
		answersTable = table.DataTable({
			data: dbdata,
			columns: [
				{
					"data": 'user_ip',
					"title": 'IP',
					"orderable": true,
					"searchable": true,
					"class": 'text-left',
					"render": function (data, type, row, meta) {
						return row.user_ip;
					}
				},
				{
					"data": 'answer_1',
					"title": 'Сколько цветов в радуге?',
					"orderable": true,
					"searchable": true,
					"class": 'text-left',
					"render": function (data, type, row, meta) {
						return row.answer_1;
					}
				},
				{
					"data": "answer_2",
					"title": 'Сколько дней в году?',
					"orderable": true,
					"searchable": true,
					"render": function (data, type, row, meta) {
						return row.answer_2;
					}
				},
				{
					"data": "answer_3",
					"title": 'Кто проживает на дне океана?',
					"orderable": true,
					"searchable": true,
					"render": function (data, type, row, meta) {
						return row.answer_3;
					}
				},
				{
					"data": "total",
					"title": 'Количество отправок теста',
					"orderable": true,
					"searchable": true,
					"render": function (data, type, row, meta) {
						return row.total;
					}
				},
				{
					"data": "options",
					"title": '',
					"orderable": false,
					"searchable": false,
					"render": function (data, type, row, meta) {
						return '<a class="text-danger deleteAnswer" style="cursor: pointer"  data-answer-ip="' + row.user_ip + '"><i class="fa fa-trash fa-2x" aria-hidden="true"></i> </a>';
					}
				}
			],

			"sDom": "<'table_feature_top' <'exports_option col-md-6 no-padding' B><'col-md-6 exports_option no-padding' f>><><'ze_wrapper't><<'col-md-6 col-xs-12'l><'col-md-6 col-xs-12'p>>",
			// "sDom": "<'table_feature_top' <'exports_option col-md-5 no-padding'<'col-md-12 no-padding'B>><'col-md-7 datatable_features'>><'ze_wrapper't><'row'<'col-xs-6 units_section'l><' col-xs-6  'p>>",
			"drawCallback": function( settings ) {

			},
			"buttons": [
				'csv',
			],
			"initComplete": function (settings, json) {

			},
			"language": {
			},
			//"bLengthChange": false,
			"iDisplayLength": 25,
			//"aoColumnDefs": [{ "bVisible": false, "aTargets": [0] }],
			"bAutoWidth": false,
			"sPaginationType": "simple_numbers",
		});

	};

	makeAnswersTable(window._userAnswers);

	$(document.body).delegate('#answersTable .deleteAnswer','click', function(ev){
		var answerIp = $(this).attr('data-answer-ip');
		$.ajax({
			url: "/admin/dashboard/delete",
			data: { answerIp: answerIp },
			type: 'POST',
			success: function(response){
				response = JSON.parse(response);
				if(response.success){
					var row = $('#answersTable tr td a[data-answer-ip="' + answerIp + '"]').parents('tr');
					answersTable.row( row ).remove().draw();
					toastr.success('', 'Удалено успешно');
				} else {
					toastr.success('', 'Error');
				}
			}
		});

	});
	var rightAnswers = {
		answer1: '7',
		answer2: 365,
		answer3: 'Медуза',
	};

	var answer1 = {count: 0};
	var answer2 = {count: 0};
	var answer3 = {count: 0};
	$.each(window._userAnswers, function(index, value) {
		if (value.answer_1.split(',').indexOf(rightAnswers['answer1']) !== -1) {
			answer1.count++;
		}
		if (value.answer_2 == rightAnswers['answer2']) {
			answer2.count++;
		}
		if (value.answer_3 == rightAnswers['answer3']) {
			answer3.count++;
		}
	});
	var total = answer1.count + answer2.count + answer3.count;
	var k = 100 / total;

	Highcharts.chart('myChart', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title: {
			text: ''
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				}
			}
		},
		series: [{
			name: 'процент',
			colorByPoint: true,
			data: [{
				name: 'Сколько цветов в радуге?',
				y: (answer1.count * k),
			}, {
				name: 'Сколько дней в году?',
				y: (answer2.count * k),
				sliced: true,
				selected: true
			}, {
				name: 'Кто проживает на дне океана?',
				y: (answer3.count * k),
			},
			]
		}]
	});
})(jQuery);
